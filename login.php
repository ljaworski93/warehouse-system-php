<?php
session_start();

if(empty($_POST['login']) || empty($_POST['password']))
{
    header('Location: index.php');
    exit();
}

require_once "connect.php";

$connection= @new mysqli($host, $db_user, $db_password, $db_name);

if($connection->connect_errno!=0)
{
    echo "Error: ".$connection->connect_errno;
}
else
{
    $login = $_POST['login'];
    $password= $_POST['password'];

    $login = $connection->real_escape_string(htmlentities($login,ENT_QUOTES, "UTF-8"));
    $password= $connection->real_escape_string(htmlentities($password,ENT_QUOTES, "UTF-8"));
	
    if(isset($_POST['admin']) && $_POST['admin'] == 'Yes')
    {
        if($result = @$connection->query("SELECT * FROM users JOIN role ON users.role_id = role.id WHERE login = '$login' AND role.name = 'admin';"))
        {
            $users = $result ->num_rows;
            $row = $result ->fetch_assoc();
            $passwordHashed = $row['password'];

            if($users == 1 and password_verify($password, $passwordHashed))
            {
                $_SESSION['id'] = $row['id'];
                $_SESSION['first_name'] = $row['first_name'];
                $_SESSION['last_name'] = $row['last_name'];
                unset($_SESSION['error']);
                $_SESSION['logged'] = true;
                $_SESSION['admin'] = true;

                header('Location: store_admin.php');
            }
            else
            {
                $_SESSION['error'] = '<span style = "color:red"> Nieprawidłowy login lub hasło!</span>';
                header('Location: index.php');
            }

            $result ->close();
            $connection->close();
        }
    }
    else
    {
        if($result = @$connection->query("SELECT * FROM users JOIN role ON users.role_id = role.id WHERE login = '$login' AND role.name = 'worker';"))
        {
            $users = $result ->num_rows;
            $row = $result ->fetch_assoc();
            $passwordHashed = $row['password'];

            if($users == 1 and password_verify($password, $passwordHashed))
            {
				$_SESSION['id'] = $row['id'];
				$_SESSION['first_name'] = $row['first_name'];
				$_SESSION['last_name'] = $row['last_name'];
				unset($_SESSION['error']);
				$_SESSION['logged'] = true;
	
				header('Location: store.php');
            }
            else
            {
                $_SESSION['error'] = '<span style = "color:red"> Nieprawidłowy login lub hasło!</span>';
                header('Location: index.php');
            }

            $result ->close();
            $connection->close();
        }
    }
}
?>