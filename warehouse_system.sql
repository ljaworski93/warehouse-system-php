-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 26 Maj 2017, 12:08
-- Wersja serwera: 10.1.21-MariaDB
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `warehouse_system`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `firm` varchar(255) DEFAULT NULL,
  `model` varchar(255) NOT NULL,
  `weight` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ean` varchar(15) NOT NULL DEFAULT '0',
  `product_category_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `firm`, `model`, `weight`, `ean`, `product_category_id`, `created_by`, `last_modified`) VALUES
(1, 'Lenovo Y700', 'Laptop do gier', 'Lenovo', 'Y700', '2.00', '1234567890123', 1, 2, '2017-05-25 18:30:45'),
(2, 'Acer Aspire 5555g', 'Laptop mutlimedialny', 'Acer', '5555g', '2.10', '1234567590323', 1, 2, '2017-05-25 18:30:58'),
(3, 'Lenovo Y710', '', 'Lenovo', 'Y710', '2.00', '1434567590323', 1, 2, '2017-04-22 18:08:08'),
(4, 'Myszka Logitech G400', '', 'Logitech', 'G400', '0.20', '1254376342342', 3, 2, '2017-05-25 18:32:56');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `product_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `product_category_id`) VALUES
(1, 'Laptopy', NULL),
(2, 'Telefony komórkowe', NULL),
(3, 'Myszki', NULL),
(4, 'Telewizory', NULL),
(5, 'Monitory LED', NULL),
(6, 'Klawiatury', NULL),
(7, 'Procesory', NULL),
(8, 'Pamieci RAM', NULL),
(9, 'Zasilacze', NULL),
(10, 'Dyski twarde', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product_delivered`
--

CREATE TABLE `product_delivered` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `product_delivered`
--

INSERT INTO `product_delivered` (`id`, `store_id`, `quantity`, `user_id`, `last_modified`) VALUES
(1, 1, 10, 2, '2017-04-22 17:50:01'),
(2, 2, 30, 2, '2017-04-22 17:51:33'),
(3, 3, 34, 2, '2017-05-25 18:33:18');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'worker');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `delivered` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `store`
--

INSERT INTO `store` (`id`, `status`, `quantity`, `delivered`, `user_id`, `product_id`, `last_modified`) VALUES
(1, 'Odebrany', 12, 10, 2, 1, '2017-04-22 17:50:01'),
(2, 'Odebrany', 55, 2, 2, 2, '2017-05-26 10:04:23'),
(3, 'Odebrany', 200, 22, 2, 4, '2017-05-26 10:02:36');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `password` varchar(60) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `role_id`, `password`, `first_name`, `last_name`, `address`, `city`, `postal_code`, `phone`) VALUES
(1, 'admin', 1, '$2y$10$ZAmxGTXfQQTnNIHbky3XVezss/orHIf3z4Ogh3x6RsVPrTDsLsyKC', 'Adam', 'Nowak', NULL, NULL, NULL, NULL),
(2, 'worker', 2, '$2y$10$6rT4fWAaDAQbOC6mYUFnnuLHlsWDVHTFwjfPGWRpoe19XYqcQ2iY6', 'Jan', 'Kowalski', 'plac Grunwaldzki 1', 'Wroclaw', '11-111', '777111222'),
(3, 'test', 2, '$2y$10$2H.m6xj4DrTMvGM.cGKb0.QFU8KVJMnjVWTEvXKAsV4fDY68dfli2', 'test', 'testowy', '', '', '', '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `firm_model_ean` (`firm`,`model`,`ean`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `porduct_category_id` (`product_category_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indexes for table `product_delivered`
--
ALTER TABLE `product_delivered`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_product_delivered_store` (`store_id`),
  ADD KEY `FK_product_delivered_users` (`user_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `product_delivered`
--
ALTER TABLE `product_delivered`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `porduct_category_id` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`);

--
-- Ograniczenia dla tabeli `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `product_category_id` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`);

--
-- Ograniczenia dla tabeli `product_delivered`
--
ALTER TABLE `product_delivered`
  ADD CONSTRAINT `FK_product_delivered_store` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  ADD CONSTRAINT `FK_product_delivered_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ograniczenia dla tabeli `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ograniczenia dla tabeli `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
