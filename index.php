<?php
session_start();

if(isset($_SESSION['logged']) && $_SESSION['logged']==true && !isset($_SESSION['admin']))
{
    header('Location:store.php');
    exit();
}
?>


<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> System magazynowy </title>
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Lato:400,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
	
	<div class="container" style="width: 406px">
		<form id="log" action="login.php" method="post">
			
			<div class="header">
				<h3>Logowanie do systemu magazynowego</h3>
				<p>Wypełnij poniższe pola</p>
			</div>          
			
			<div class="sep">
			</div>
			
			<div class="inputs">
				<input type="text" name="login" placeholder="Login" autofocus />
				<input type="password" name="password" placeholder="Hasło" autofocus /> <br />	
				<input type="checkbox" name="admin" value="Yes"> Administrator <br />
				<input type="submit" value="Zaloguj się" />
				<div class="error">
					<?php
                    if(isset($_SESSION['error']))
                    {
                        echo $_SESSION['error'];
                    }						
                    ?> 
				</div>
			</div>
			
		</form>
	</div>
	
</body>
</html>