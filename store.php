<?php
session_start();

if( !isset($_SESSION['logged']) || isset($_SESSION['admin']))
{
    header('Location: index.php');
    exit();
}
?>

<?php
$nameErr = $descriptionErr = $firmErr = $modelErr = $weightErr = $eanErr = $categoryErr = $insertProductInfo = $existingProductErr = $quantityErr = $registerProductInfo = "";
$name = $description = $firm = $model = $weight = $ean = $quantity =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $canAddProduct = true;
    $canRegisterProduct = true;

    if(empty($_POST['quantity']))
    {
        $canRegisterProduct = false;
        $registerProductVisibility = false;
    }
    else
    {
        if (empty($_POST["quantity"]))
        {
            $quantityErr= "Pole ilość jest wymagane";
            $canRegisterProduct = false;
        }
        else
        {
            $quantity= test_input($_POST["quantity"]);

            if (!preg_match("/^[0-9]*$/",$quantity))
            {
                $quantityErr = "Pole ilość zawiera nieprawidłowy znak";
                $canRegisterProduct = false;
            }
        }

        $registerProductVisibility = true;
        $_SESSION['release_product_visibility'] = false;
        $productId = $_POST['existing_product'];
        $userId =  $_SESSION['id'];

        if($canRegisterProduct == true)
        {
            require_once "connect.php";

            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

            if($connection->connect_errno!=0)
            {
                echo "Error: ".$connection->connect_errno;
            }
            else
            {
                $insertResult = @$connection->real_query("INSERT INTO store (status, quantity, user_id, product_id) VALUES ('Odebrany', '$quantity', '$userId', '$productId');");

                if($insertResult)
                {
                    $registerProductInfo = "Zarejestrowano produkt";
                    unset($_POST);
                    unset($_REQUEST);
                    $quantity =  "";
                }
                else
                {
                    $registerProductInfo = "Blad podczas rejestracji produktu";
                }

                $connection->close();
            }
        }
    }

    if(empty($_POST["name"]) && empty($_POST["firm"]) && empty($_POST["model"]) && empty($_POST["ean"]))
    {
        $canAddProduct = false;
        $addProductVisibility = false;
    }
    else
    {
        if (empty($_POST["name"]))
        {
            $nameErr= "Pole nazwa jest wymagane";
            $canAddProduct = false;
        }
        else
        {
            $name= test_input($_POST["name"]);

            if (!preg_match("/^[a-zA-Z0-9 ]*$/",$name))
            {
                $nameErr = "Pole nazwa zawiera nieprawidłowy znak";
                $canAddProduct = false;
            }
        }

        if (empty($_POST["firm"]))
        {
            $firmErr = "Pole firma jest wymagane";
            $canAddProduct = false;
        }
        else
        {
            $firm = test_input($_POST["firm"]);

            if (!preg_match("/^[0-9a-zA-Z- ]*$/",$firm))
            {
                $firmErr = "Pole firma zawiera nieprawidłowy znak";
                $canAddProduct = false;
            }
        }

        if (empty($_POST["model"]))
        {
            $modelErr = "Pole model jest wymagane";
            $canAddProduct = false;
        }
        else
        {
            $model = test_input($_POST["model"]);

            if (!preg_match("/^[0-9a-zA-Z- ]*$/",$model))
            {
                $modelErr = "Pole model zawiera nieprawidłowy znak";
                $canAddProduct = false;
            }
        }

        if (!empty($_POST["weight"]))
        {
            $weight = test_input($_POST["weight"]);

            if (!preg_match("/^[0-9.]*$/",$weight))
            {
                $weightErr = "Pole waga zawiera nieprawidłowy znak";
                $canAddProduct = false;
            }
        }

        if (empty($_POST["ean"]))
        {
            $eanErr = "Pole ean jest wymagane";
            $canAddProduct = false;
        }
        else
        {
            $ean = test_input($_POST["ean"]);

            if (!preg_match("/^[0-9]*$/",$ean))
            {
                $eanErr = "Pole ean zawiera nieprawidłowy znak";
                $canAddProduct = false;
            }
        }

        $addProductVisibility = true;
        $registerProductVisibility = false;
        $_SESSION['release_product_visibility'] = false;
        $product_category = $_POST['product_category'];
        $userId =  $_SESSION['id'];

        if($canAddProduct == true)
        {
            require_once "connect.php";

            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

            if($connection->connect_errno!=0)
            {
                echo "Error: ".$connection->connect_errno;
            }
            else
            {
                $insertResult = @$connection->real_query("INSERT INTO product (name, description, firm, model, weight, ean, product_category_id, created_by) VALUES ('$name', '$description', '$firm', '$model', '$weight', '$ean', '$product_category', '$userId');");

                if($insertResult)
                {
                    $insertProductInfo = "Dodano produkt";
                    $_POST = array();
                    $name = $description = $firm = $model = $weight = $ean =  "";
                }
                else
                {
                    $insertProductInfo = "Blad podczas dodawania produktu";
                }

                $connection->close();
            }
        }
    }
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> System magazynowy </title>
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Lato:400,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	
	<script>
	
		function openCity(evt, cityName) 
		{
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
	</script>	
</head>

<body>
	<div class="container">	
		<div class="logo">
			<div>
				<?php
				echo '<a class="button button4" href="logout.php" >Wyloguj</a>';
                ?>      
			</div>
            <h5>Twoje konto (pracownik)</h5>					
		</div>
		
		<div class="menu">
			<div class="option">
				<?php
				echo "<center> Zalogowany jako: ".$_SESSION['first_name']." ".$_SESSION['last_name'];
                ?>
			</div>	
			<div class="tab">
                <button class="tablinks" onclick="openCity(event, 'ReleaseProduct')">Wydaj produkt</button>
                <button class="tablinks" onclick="openCity(event, 'RegisterProduct')">Zarejestruj produkt</button> 
			    <button class="tablinks" onclick="openCity(event, 'NewProduct')">Dodaj nowy produkt</button>
                <button class="tablinks" onclick="openCity(event, 'DeliveredProducts')">Wydane produkty</button>
			</div>

            <div id="ReleaseProduct" class="tabcontent" style="<?php echo (isset($_SESSION['release_product_visibility']) &&  $_SESSION['release_product_visibility'] == true)?'display:block':'display:none';?>">
                <h3>Wydaj produkt</h3>
                <h4>Wypełnij formularz</h4>
                <p>
                    <span class="error">* wymagane pola.</span>
                </p>

                <div class="form">
                    <form method="post" action="release_product.php">
                        <label for="product_to_release">Produkt</label>
                        <select id="product_to_release" name="product_to_release">
                            <?php
                            require_once "connect.php";

                            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                            if($connection->connect_errno!=0)
                            {
                                echo "Error: ".$connection->connect_errno;
                            }
                            else
                            {
                                $productsToRelease = @$connection->query("SELECT store.id, store.`status`, (store.quantity - store.delivered) as quantity, product.name AS product_name, CONCAT(users.first_name, ' ', users.last_name) AS user_name FROM store JOIN product ON product.id = store.product_id JOIN users ON users.id = store.user_id WHERE STATUS = 'Odebrany';");

                                while($productToRelease = $productsToRelease->fetch_assoc())
                                {
                                    echo "<option value=\"".$productToRelease['id']."\">Status: ".$productToRelease['status'].", Nazwa: ".$productToRelease['product_name'].", Ilość: ".$productToRelease['quantity'].", Użytkownik: ".$productToRelease['user_name']."</option>";
                                }
                            }
                            $connection->close();
                            ?>
                        </select>

                        <label for="quantity">Ilość</label>
                        <input type="text" id="quantity" name="quantity" placeholder="Ilość.." />

                        <input type="submit" name="releaseProduct" value="Wydaj" />
                        <span class="error">
                            <?php
                            if(isset($_SESSION['release_product_info']))
                            {
                                echo $_SESSION['release_product_info'];
                            }
                            ?>
                        </span>
                        <br />
                        <br />
                    </form>
                </div>
            </div>

            <div id="NewProduct" class="tabcontent" style="<?php echo ($addProductVisibility)?'display:block':'display:none';?>">
                <h3>Dodaj nowy produkt</h3>
                <h4>Wypełnij formularz</h4>
                <p>
                    <span class="error">* wymagane pola.</span>
                </p>

                <div class="form">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <label for="name">Nazwa</label>
                        <span class="error">
                            * <?php echo $nameErr;?>
                        </span>
                        <input type="text" id="name" name="name" placeholder="Nazwa.." value="<?php echo (isset($name))?$name:'';?>" />

                        <label for="description">Opis</label>
                        <span class="error">
                            <?php echo $descriptionErr;?>
                        </span>
                        <input type="text" id="description" name="description" placeholder="Opis.." value="<?php echo (isset($description))?$description:'';?>" />

                        <label for="firm">Firma</label>
                        <span class="error">
                            * <?php echo $firmErr;?>
                        </span>
                        <input type="text" id="firm" name="firm" placeholder="Firma.." value="<?php echo (isset($firm))?$firm:'';?>" />

                        <label for="model">Model</label>
                        <span class="error">
                            * <?php echo $modelErr;?>
                        </span>
                        <input type="text" id="model" name="model" placeholder="Model.." value="<?php echo (isset($model))?$model:'';?>" />

                        <label for="weight">Waga</label>
                        <span class="error">
                            <?php echo $weightErr;?>
                        </span>
                        <input type="text" id="weight" name="weight" placeholder="Waga.." value="<?php echo (isset($weight))?$weight:'';?>" />

                        <label for="ean">Ean</label>
                        <span class="error">
                            * <?php echo $eanErr;?>
                        </span>
                        <input type="text" id="ean" name="ean" placeholder="Ean.." value="<?php echo (isset($ean))?$ean:'';?>" />

                        <label for="role_name">Kategoria produktu</label>
                        <span class="error">
                            * <?php echo $categoryErr;?>
                        </span>
                        <select id="product_category" name="product_category">
                            <?php
                            require_once "connect.php";

                            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                            if($connection->connect_errno!=0)
                            {
                                echo "Error: ".$connection->connect_errno;
                            }
                            else
                            {
                                $categories = @$connection->query("SELECT id, name FROM product_category;");

                                while($category = $categories->fetch_assoc())
                                {
                                    echo "<option value=\"".$category['id']."\">".$category['name']."</option>";
                                }
                            }
                            $connection->close();
                            ?>
                        </select>

                        <input type="submit" name="insertProduct" value="Zapisz" />
                        <span class="error">
                            <?php echo $insertProductInfo;?>
                        </span>
                        <br />
                        <br />
                    </form>
                </div>
            </div>	

            <div id="RegisterProduct" class="tabcontent" style="<?php echo ($registerProductVisibility)?'display:block':'display:none';?>">
                <h3>Zarejestruj istniejący produkt</h3>
                <h4>Wypełnij formularz</h4>
                <p>
                    <span class="error">* wymagane pola.</span>
                </p>

                <div class="form">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <label for="existing_product">Produkt</label>
                        <span class="error">
                            * <?php echo $existingProductErr;?>
                        </span>
                        <select id="existing_product" name="existing_product">
                            <?php
                            require_once "connect.php";

                            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                            if($connection->connect_errno!=0)
                            {
                                echo "Error: ".$connection->connect_errno;
                            }
                            else
                            {
                                $existingProducts = @$connection->query("SELECT id, CONCAT(name, ' (', firm, '-', model, ') ') AS product_name FROM product;");

                                while($existingProduct = $existingProducts->fetch_assoc())
                                {
                                    echo "<option value=\"".$existingProduct['id']."\">".$existingProduct['product_name']."</option>";
                                }
                            }
                            $connection->close();
                            ?>
                        </select>

                        <label for="quantity">Ilość</label>
                        <span class="quantity">
                            * <?php echo $quantityErr;?>
                        </span>
                        <input type="text" id="quantity" name="quantity" placeholder="Ilość.." value="<?php echo (isset($quantity))?$quantity:'';?>" />

                        <input type="submit" name="registerProduct" value="Zapisz" />
                        <span class="error">
                            <?php echo $registerProductInfo;?>
                        </span>
                        <br />
                        <br />
                    </form>
                </div>
            </div>
            
            <div id="DeliveredProducts" class="tabcontent" style="display:none">
                <h3>Lista wydanych produktow</h3>
                <table border="1" cellpadding="1" cellspacing="1">
                    <tr>
                        <th>Nazwa</th>
                        <th>Ilość</th>
                        <th>Użytkownik</th>
                        <th>Czas</th>
                    </tr>
                    <?php
                    require_once "connect.php";

                    $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                    if($connection->connect_errno!=0)
                    {
                        echo "Error: ".$connection->connect_errno;
                    }
                    else
                    {
                        $records = @$connection->query("SELECT product_delivered.quantity, product_delivered.last_modified, product.name AS product_name, CONCAT(users.first_name, ' ', users.last_name) AS user_name FROM product_delivered JOIN users ON users.id = product_delivered.user_id JOIN store ON store.id = product_delivered.store_id JOIN product ON product.id = store.product_id;");

                        while($row = $records->fetch_assoc())
                        {
                            echo "<tr>";
                            echo "<td>".$row['product_name']."</td>";
                            echo "<td>".$row['quantity']."</td>";
                            echo "<td>".$row['user_name']."</td>";
                            echo "<td>".$row['last_modified']."</td>";
                            echo "</tr>";
                        }
                    }

                    $connection->close();
                    ?>
                </table>
            </div>		
			<div style="clear:both;"></div>				
		</div>	
	</div>
</body>
</html>