<?php
session_start();

if (!isset($_SESSION['logged']) || !isset($_SESSION['admin']))
{
    header('Location: index.php');
    exit();
}
?>

<?php
$loginErr = $passwordErr= $first_nameErr= $postal_codeErr= $last_nameErr= $addressErr= $cityErr= $postal_codeErr= $phoneErr= $role_nameErr= $insertUserInfo= "";
$login = $last_name= $password= $first_name= $postal_code= $last_name= $address= $city= $postal_code= $phone= $role_name= "";

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $canSend = true;

    if (empty($_POST["login"]))
    {
        $loginErr= "Pole login jest wymagane";
        $canSend = false;
    }
    else
    {
        $login= test_input($_POST["login"]);

        if (!preg_match("/^[0-9a-zA-Z]*$/",$login))
        {
            $loginErr = "Pole login zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (empty($_POST["password"]))
    {
        $passwordErr= "Pole hasło jest wymagane";
        $canSend = false;
    }
    else
    {
        $password= test_input($_POST["password"]);

        if(strlen($password) < 3)
        {
            $passwordErr = "Hasło jest za krótkie";
            $canSend = false;
        }
    }

    if (empty($_POST["first_name"]))
    {
        $first_nameErr= "Pole imię jest wymagane";
        $canSend = false;
    }
    else
    {
        $first_name= test_input($_POST["first_name"]);

        if (!preg_match("/^[a-zA-Z ]*$/",$first_name))
        {
            $first_nameErr = "Pole imię zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (empty($_POST["last_name"]))
    {
        $last_nameErr= "Pole nazwisko jest wymagane";
        $canSend = false;
    }
    else
    {
        $last_name= test_input($_POST["last_name"]);

        if (!preg_match("/^[a-zA-Z ]*$/",$last_name))
        {
            $last_nameErr = "Pole nazwisko zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (!empty($_POST["address"]))
    {
        $address = test_input($_POST["address"]);

        if (!preg_match("/^[0-9a-zA-Z ]*$/",$address))
        {
            $addressErr = "Pole adres zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (!empty($_POST["city"]))
    {
        $city = test_input($_POST["city"]);

        if (!preg_match("/^[a-zA-Z ]*$/",$city))
        {
            $cityErr = "Pole miasto zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (!empty($_POST["postal_code"]))
    {
        $postal_code = test_input($_POST["postal_code"]);

        if (!preg_match("/^[0-9- ]*$/",$postal_code))
        {
            $postal_codeErr = "Pole kod pocztowy zawiera nieprawidłowy znak";
            $canSend = false;
        }
    }

    if (!empty($_POST["phone"]))
    {
        $phone = test_input($_POST["phone"]);

        if (!preg_match("/^[0-9- ]*$/",$phone))
        {
            $phoneErr = "Pole telefon zawiera nieprawidłowy znak";
            $canSend = false;
        }

        if(strlen($phone) > 9)
        {
            $phoneErr = "Pole telefon posiada nieprawidłową długość";
            $canSend = false;
        }
    }

    if(empty($login) && empty($password) && empty($first_name) && empty($last_name))
    {
        $addUserFormVisibility = false;
    }
    else
    {
        $addUserFormVisibility = true;
    }

    $role_name = $_POST["role_name"];

    if($canSend == true)
    {
        require_once "connect.php";

        $connection= @new mysqli($host, $db_user, $db_password, $db_name);

        if($connection->connect_errno!=0)
        {
            echo "Error: ".$connection->connect_errno;
        }
        else
        {
            if($result = @$connection->query("SELECT * FROM users JOIN role ON users.role_id = role.id WHERE login = '$login' AND PASSWORD = '$password' AND role.name = 'admin';"))
            {
                $users = $result ->num_rows;

                if($users > 0)
                {
                    $loginErr = "Użytkownik posiadający taki login już istnieje";
                }
                else
                {
					$passwordHashed = password_hash($password, PASSWORD_DEFAULT); //password hashing
                    $insertResult = @$connection->real_query("INSERT INTO users (login, role_id, password, first_name, last_name, address, city, postal_code, phone) VALUES ('$login', '$role_name', '$passwordHashed', '$first_name', '$last_name', '$address', '$city', '$postal_code', '$phone');");

                    if($insertResult)
                    {
                        $insertUserInfo = "Dodano użytkownika";
                        $_POST = array();
                        $login = $last_name= $password= $first_name= $postal_code= $last_name= $address= $city= $postal_code= $phone= $role_name= "";
                    }
                    else
                    {
                        $insertUserInfo = "Blad podczas dodawania użytkownika";
                    }
                }
            }
        }
    }
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> System magazynowy </title>
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Lato:400,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	
	<script>
	
		function openCity(evt, cityName) 
		{
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
	</script>	
</head>

<body>
	<div class="container">	
		<div class="logo">
			<div>
				<?php
				echo '<a class="button button3" href="logout.php" >Wyloguj</a>';
                ?>      
			</div>
            <h5>Twoje konto (administrator)</h5>					
		</div>
		
		<div class="menu">
			<div class="option">
				<?php
				echo "<center> Zalogowany jako: ".$_SESSION['first_name']." ".$_SESSION['last_name'];
                ?>
			</div>	
			<div class="tab">
			  <button class="tablinks" onclick="openCity(event, 'AddUser')">Dodaj użytkownika</button>
			  <button class="tablinks" onclick="openCity(event, 'ReviewUsers')">Przeglądaj użytkowników</button>			  		
			</div>

			<div id="AddUser" class="tabcontent" style="<?php echo ($addUserFormVisibility)?'display:block':'display:none';?>">
				<h3>Nowy użytkownik</h3>
				<h4>Wypełnij formularz</h4>
				<p><span class="error">* wymagane pola.</span></p>

                <div class="form">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">                        
                        <label for="login">Login</label>
                        <span class="error">
                            * <?php echo $loginErr;?>
                        </span>
                        <input type="text" id="login" name="login" placeholder="Login.." value="<?php echo (isset($login))?$login:'';?>" />
                        
                        <label for="password">Hasło</label>
                        <span class="error">
                            * <?php echo $passwordErr;?>
                        </span>
                        <input type="text" id="password" name="password" placeholder="Hasło.." value="<?php echo (isset($password))?$password:'';?>" type='password' />
                        
                        <label for="first_name">Imię</label>
                        <span class="error">
                            * <?php echo $first_nameErr;?>
                        </span>
                        <input type="text" id="first_name" name="first_name" placeholder="Imię.." value="<?php echo (isset($first_name))?$first_name:'';?>" />
                        
                        <label for="last_name">Nazwisko</label>
                        <span class="error">
                            * <?php echo $last_nameErr;?>
                        </span>
                        <input type="text" id="last_name" name="last_name" placeholder="Nazwisko.." value="<?php echo (isset($last_name))?$last_name:'';?>" />

                        <label for="address">Adres</label>
                        <span class="error">
                             <?php echo $addressErr;?>
                        </span>
                        <input type="text" id="address" name="address" placeholder="Adres.." value="<?php echo (isset($address))?$address:'';?>" />

                        <label for="city">Miasto</label>
                        <span class="error">
                             <?php echo $cityErr;?>
                        </span>
                        <input type="text" id="city" name="city" placeholder="Miasto.." value="<?php echo (isset($city))?$city:'';?>" />

                        <label for="postal_code">Kod pocztowy</label>
                        <span class="error">
                             <?php echo $postal_codeErr;?>
                        </span>
                        <input type="text" id="postal_code" name="postal_code" placeholder="Kod pocztowy.."  value="<?php echo (isset($postal_code))?$postal_code:'';?>" />

                        <label for="phone">Telefon</label>
                        <span class="error">
                             <?php echo $phoneErr;?>
                        </span>
                        <input type="text" id="phone" name="phone" placeholder="Telefon.." value="<?php echo (isset($phone))?$phone:'';?>" /> 

                        <label for="role_name">Typ użytkownika</label>
                        <span class="error">
                            * <?php echo $role_nameErr;?>
                        </span>
                        <select id="role_name" name="role_name"> 
                            <?php
                            require_once "connect.php";

                            $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                            if($connection->connect_errno!=0)
                            {
                                echo "Error: ".$connection->connect_errno;
                            }
                            else
                            {
                                $roles = @$connection->query("SELECT id, name FROM role;");

                                while($role = $roles->fetch_assoc())
                                {
                                    echo "<option value=\"".$role['id']."\">".$role['name']."</option>";
                                }
                            }
                            $connection->close();
                            ?>                                              
                        </select>
                        
                        <input type="submit" name="insertUser" value="Zapisz" /> 
                        <span class="error"><?php echo $insertUserInfo;?>
                        </span> <br /> <br />                                       
                    </form>
                 </div>	
			</div>

			<div id="ReviewUsers" class="tabcontent" style="display:none">
			  <h3>Lista użytkowników</h3>
                <table border="1" cellpadding="1" cellspacing="1">
                    <tr>
                        <th>Login</th>
                        <th>Imię</th>
                        <th>Nazwisko</th>
                        <th>Adres</th>
                        <th>Miasto</th>
                        <th>Kod pocztowy</th>
                        <th>Telefon</th>
                        <th>Typ użytkownika</th>
                    </tr>
                    <?php     
                    require_once "connect.php";

                    $connection= @new mysqli($host, $db_user, $db_password, $db_name);

                    if($connection->connect_errno!=0)
                    {
                        echo "Error: ".$connection->connect_errno;
                    }
                    else
                    {  
                        $records = @$connection->query("SELECT users.login, users.first_name, users.last_name, users.address, users.city, users.postal_code, users.phone, role.name FROM users JOIN role ON users.role_id = role.id;");   
                        
                        while($user = $records->fetch_assoc())
                        {
                            echo "<tr>";
                            echo "<td>".$user['login']."</td>";
                            echo "<td>".$user['first_name']."</td>";
                            echo "<td>".$user['last_name']."</td>";
                            echo "<td>".$user['address']."</td>";
                            echo "<td>".$user['city']."</td>";
                            echo "<td>".$user['postal_code']."</td>";
                            echo "<td>".$user['phone']."</td>";
                            echo "<td>".$user['name']."</td>";
                            echo "</tr>";                            
                        }
                    }
                    
                    $connection->close();
                    ?>
                </table>             
			</div>			
			<div style="clear:both;"></div>				
		</div>	
	</div>
</body>
</html>