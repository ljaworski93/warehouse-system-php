<?php
session_start();

$_SESSION['release_product_info'] = '';

if(empty($_POST['quantity']))
{
    header('Location: store.php');
    exit();
}

require_once "connect.php";

$connection= @new mysqli($host, $db_user, $db_password, $db_name);

if($connection->connect_errno!=0)
{
    echo "Error: ".$connection->connect_errno;
}
else
{
    if (empty($_POST["quantity"]))
    {
        $quantityErr= "Pole ilo�� jest wymagane";
        $canRegisterProduct = false;
        $_SESSION['release_product_visibility'] = false;
    }
    else
    {
        $quantity= test_input($_POST["quantity"]);

        if (!preg_match("/^[0-9]*$/",$quantity))
        {
            $quantityErr = "Pole ilo�� zawiera nieprawid�owy znak";
            $canRegisterProduct = false;
        }

        $_SESSION['release_product_visibility'] = true;

        if($quantity > 0)
        {
            $storeId = $_POST['product_to_release'];
            $userId =  $_SESSION['id'];

            if($result = @$connection->query("SELECT quantity, delivered FROM store WHERE id = '$storeId';"))
            {
                $row= $result ->fetch_assoc();

                $store_quantity = $row['quantity'];
                $store_delivered = $row['delivered'];

                if($quantity == ($store_quantity - $store_delivered))
                {
                    @$connection->real_query("INSERT INTO product_delivered (store_id, quantity, user_id) VALUES ('$storeId', '$quantity', '$userId');");
                    @$connection->real_query("UPDATE store SET delivered = '$quantity', status = 'Wydany' WHERE id = '$storeId';");
                    $_SESSION['release_product_info'] = "Wydano produkt";
                }
                else if($quantity < ($store_quantity - $store_delivered))
                {
                    @$connection->real_query("INSERT INTO product_delivered (store_id, quantity, user_id) VALUES ('$storeId', '$quantity', '$userId');");
                    @$connection->real_query("UPDATE store SET delivered = '$quantity' WHERE id = '$storeId';");
                    $_SESSION['release_product_info'] = "Wydano produkt";
                }
                else
                {
                    $_SESSION['release_product_info'] = "Blad podczas wydawania produktu";
                }

                @$connection->close();
            }
        }
    }
}

header('Location: store.php');
exit();

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>